section .data

buffer: dd  0, 0, 0, 0 ,0, 0, 0, 0
output: dd 0

section .text

global AVXHorizontalSum32

;inputs must be 8 32-bit floating points placed in ymm0
;output will be in xmm0 as a single floating point

AVXHorizontalSum32:
    vmovaps [buffer], ymm0
    fldz

    fld dword [buffer]
    faddp st1

    fld dword [buffer+4]
    faddp st1

    fld dword [buffer+8]
    faddp st1

    fld dword [buffer+12]
    faddp st1

    fld dword [buffer+16]
    faddp st1

    fld dword [buffer+20]
    faddp st1

    fld dword [buffer+24]
    faddp st1

    fld dword [buffer+28]
    faddp st1

    fst dword [output]
    movaps xmm0, [output]

    ;clear all buffered data
    ;for security issues
    mov dword [output], 0
    mov dword [buffer], 0
    mov dword [buffer+4], 0
    mov dword [buffer+8], 0
    mov dword [buffer+12], 0
    mov dword [buffer+16], 0
    mov dword [buffer+20], 0
    mov dword [buffer+24], 0
    mov dword [buffer+28], 0

    ret