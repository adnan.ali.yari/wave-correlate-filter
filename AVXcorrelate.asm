segment .data

;saving input to memory 
input: dq 0
input_size: dq 0
filter: dq 0
filter_size: dq 0
output: dq 0

;buffer for special situations
filter_buffer: dd 0, 0, 0, 0, 0, 0, 0, 0
input_buffer: dd 0, 0, 0, 0, 0, 0, 0, 0

;saving reserved registers
rbx_: dq 0 
rbp_: dq 0
r12_: dq 0
r13_: dq 0
r14_: dq 0
r15_: dq 0

segment .text

global correlate
extern AVXHorizontalSum32

correlate:
    enter 0,0
    
    mov [input], rdi 
    mov [input_size], rsi
    mov [filter], rdx
    mov [filter_size], rcx
    mov [output], r8 
    
    mov [r12_], r12
    mov [r13_], r13
    mov [r14_], r14
    mov [r15_], r15

    dec rsi
    dec rcx

    mov r9, 0


    outer_loop:
    mov r10, rcx
    mov r11, rcx
    mov r12, rsi
    mov r13, r9

    sar r10, 1
    sub r10, r9
    cmp r10, 0
    jl continue
    mov r10, 0

    continue: 
    sub r11, r10
    inc r11 

    sub r12, r9
    inc r12
        
    fldz
        inner_loop:
        
        cmp r11, 8
        jge filter_greater_or_equal_8
        
        mov ebx, 0

        filter_buffer_loop:
        cmp r11, 0
        jle set_element_to_zero
        mov ebp, [rdx+r10*4]
        jmp insert_zero_to_filter 
        set_element_to_zero:
        mov ebp, 0
        insert_zero_to_filter:
        mov [filter_buffer+ebx*4], ebp
        inc ebx
        dec r11
        cmp ebx, 8
        jl filter_buffer_loop
        vmovaps ymm0, [filter_buffer]
        jmp filter_checked
        filter_greater_or_equal_8:
        vmovaps ymm0, [rdx+r10*4]
        filter_checked:



        cmp r12, 8
        jge filter_greater_or_equal_8

        mov ebx, 0

        input_buffer_loop:
        cmp r12, 0
        jle set_element_to_zero_
        mov ebp, [rdi+r13*4]
        jmp insert_zero_to_input 
        set_element_to_zero_:
        mov ebp, 0
        insert_zero_to_input:
        mov [filter_buffer+ebx*4], ebp
        inc ebx
        dec r12
        cmp ebx, 8
        jl input_buffer_loop
        vmovaps ymm1, [filter_buffer]
        jmp input_checked
        input_greater_or_equal_8:
        vmovaps ymm1, [rdi+r13*4]
        input_checked:

        vmulps ymm0, ymm0, ymm1
        call AVXHorizontalSum32
        faddp st1

        add r13, 8
        add r10, 8
        sub r11, 8
        cmp r11, 0
        jle inner_loop
        sub r12, 8
        cmp r12, 0 
        jle inner_loop


        fld dword [output]
        mov [rdi+r9*4]


    inc r9
    cmp rsi, r9
    jge outer_loop



    leave
    ret


