	.file	"test.c"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$168, %edi
	call	compare
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.globl	compare
	.type	compare, @function
compare:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -4(%rbp)
	cmpl	$256, -4(%rbp)
	jg	.L4
	cmpl	$223, -4(%rbp)
	jle	.L4
	movl	$8, %eax
	jmp	.L5
.L4:
	cmpl	$223, -4(%rbp)
	jg	.L6
	cmpl	$191, -4(%rbp)
	jle	.L6
	movl	$7, %eax
	jmp	.L5
.L6:
	cmpl	$191, -4(%rbp)
	jg	.L7
	cmpl	$159, -4(%rbp)
	jle	.L7
	movl	$6, %eax
	jmp	.L5
.L7:
	cmpl	$159, -4(%rbp)
	jg	.L8
	cmpl	$127, -4(%rbp)
	jle	.L8
	movl	$5, %eax
	jmp	.L5
.L8:
	cmpl	$127, -4(%rbp)
	jg	.L9
	cmpl	$95, -4(%rbp)
	jle	.L9
	movl	$4, %eax
	jmp	.L5
.L9:
	cmpl	$95, -4(%rbp)
	jg	.L10
	cmpl	$63, -4(%rbp)
	jle	.L10
	movl	$3, %eax
	jmp	.L5
.L10:
	cmpl	$63, -4(%rbp)
	jg	.L11
	cmpl	$31, -4(%rbp)
	jle	.L11
	movl	$2, %eax
	jmp	.L5
.L11:
	cmpl	$31, -4(%rbp)
	jg	.L12
	cmpl	$0, -4(%rbp)
	js	.L12
	movl	$1, %eax
	jmp	.L5
.L12:
	movl	$0, %eax
.L5:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	compare, .-compare
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
